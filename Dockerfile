FROM alpine:3.7

ENTRYPOINT ["/bin/bash", "-l", "-c"]

RUN set -x                  && \
    apk --no-cache add curl bash && \
    apk --update upgrade    && \
    apk add ca-certificates && \
    rm -rf /var/cache/apk/*

ENV K8S_VERSION 1.10.6
ENV HELM_VERSION "2.9.1"
ENV HELM_URL "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz"


RUN set -x && \
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl  && \
    chmod +x ./kubectl && \
    mv ./kubectl /bin/kubectl

RUN curl -s ${HELM_URL} | tar zxf - && \
    mv linux-amd64/helm /usr/bin/ && \
    chmod +x /usr/bin/helm
